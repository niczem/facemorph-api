import os, time, dlib, face_recognition, facemorpher, sys,subprocess
from skimage import io
from flask import Flask, render_template, request, json
from PIL import Image

app = Flask(__name__)

UPLOAD_FOLDER = os.path.basename('uploads')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload_file():
    file = request.files['image']

    extension = os.path.splitext(file.filename)[1]
    print(extension)
    newname = str(time.time())+extension
    print(newname)

    f = os.path.join(app.config['UPLOAD_FOLDER'], newname)
    file.save(f)

    #check if image is RGB
    im = Image.open(f)
    if im.mode != 'RGB':
        img_to_rgb(f)

    print('counting numbers of faces')
    number_of_faces = count_faces(f)
    print('...done. '+str(number_of_faces)+' faces found')


    if number_of_faces == 0:
        return return_json({'success': False, 'message': 'No face detected'})
    if number_of_faces > 1:
        return return_json({'success': False, 'message': 'No face detected'})
    else:
        filepath = morph_image(f)
        return return_json({
        'success': True,
        'upload': filepath, 
        'morph': filepath,
        'match': filepath,
        'morph_all': filepath,
        'match_all': filepath})
        #{
        #'success': True,
        #'upload': fp_upload_static, 
        #'morph': fp_glow_static,
        #'match': fp_match_static,
        #'morph_all': fp_morph_static_all,
        #'match_all': fp_match_static_all}



def run_tests(input_directory,output_directory):
    input_files = get_directory_files(input_directory)
    img_prefix = '../../'

    html = '<html><head><title>facemorph test</title></head><body><table><tbody>'

    html += '<tr>'
    html += '<td>Original</td>'
    html += '</tr>'
    for file in input_files[1]:
        file = os.path.join(file)
        print('running test for '+file)

        #check if image is RGB
        im = Image.open(file)
        if im.mode != 'RGB':
            print('wrong mode! transforming to rgb from '+im.mode)
            img_to_rgb(file)

        #find most matching face first:
        print('counting numbers of faces')
        number_of_faces = count_faces(file)
        print('found:');
        print(number_of_faces);
        if(number_of_faces == 1):
            print('start morphing');
            lowest_distance_image, highest_distance_image = get_face_distances(file)
            mode1 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=1)
            mode2 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=2)
            mode3 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=3)
            mode4 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=4)
            mode5 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=5)
            mode6 = morph_images(lowest_distance_image, file, output_directory=output_directory, mode=6)



            html += '<tr>'
            html += '<td><img src="'+img_prefix+file+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode1+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode2+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode3+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode4+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode5+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td><img src="'+img_prefix+mode6+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '</tr>'
        else:
            print('skipping');
            html += '<tr>'
            html += '<td><img src="'+img_prefix+file+'" style="max-width:200px;image-orientation: from-image;"></td>'
            html += '<td>no face found</td>'
            html += '</tr>'

    text_file = open(output_directory+"index.html", "w")
    text_file.write(html)
    text_file.close()

    return 0

def return_json(data):
    response = app.response_class(
        response=json.dumps(data),
        status=200,
        mimetype='application/json'
    )
    return response


def count_faces(image):
    img = io.imread(image)

    detector = dlib.get_frontal_face_detector()
    # Run the face detector, upsampling the image 1 time to find smaller faces.
    dets = detector(img,1)
     
    print("number of faces detected:")
    return len(dets)

def img_to_rgb(image):
    png = Image.open(image)
    png.load() # required for png.split()

    background = Image.new("RGB", png.size, (255, 255, 255))
    background.paste(png, mask=png.split()[3]) # 3 is the alpha channel

    background.save(image, 'JPEG', quality=95)

def get_directory_files(directory):
    known_images_folder = directory

    known_images = []

    filenames = []

    #find all image files in the upload folder
    for root, dirs, files in os.walk(known_images_folder):
        for file in files:
            if file.endswith(".jpg"):
                 filenames.append(os.path.join(root, file))
                 known_images.append(face_recognition.load_image_file(os.path.join(root, file)))
    return known_images,filenames


def get_known_images():

    known_images_folder = "./identities"

    return get_directory_files(known_images_folder)

def find_most_matching_face(image):

    known_images = get_known_images

    # Load the jpg files into numpy arrays
    unknown_image = face_recognition.load_image_file(image)

    # Get the face encodings for each face in each image file
    # Since there could be more than one face in each image, it returns a list of encodings.
    # But since I know each image only has one face, I only care about the first encoding in each image, so I grab index 0.
    try:
        known_images_encoded = []
        for known_image in known_images:
            known_images_encoded.append(face_recognition.face_encodings(known_image)[0])

        #biden_face_encoding = face_recognition.face_encodings(biden_image)[0]
        #obama_face_encoding = face_recognition.face_encodings(obama_image)[0]
        unknown_face_encoding = face_recognition.face_encodings(unknown_image)[0]
    except IndexError:
        print("I wasn't able to locate any faces in at least one of the images. Check the image files. Aborting...")
        quit()



    # results is an array of True/False telling if the unknown face matched anyone in the known_faces array
    results = face_recognition.compare_faces(known_images_encoded, unknown_face_encoding)

    index = -1
    final_results = []
    for result in results:
        index+=1
        if result == True:
            final_results.append(filenames[index].split("_")[0])

    print(json.dumps(final_results))


def get_face_distances(image):


    image_to_test = face_recognition.load_image_file(image)
    image_to_test_encoding = face_recognition.face_encodings(image_to_test)[0]

    #load images from identities directory
    known_images,filenames = get_known_images()

    known_images_encoded = []
    try:
        for known_image in known_images:
            known_images_encoded.append(face_recognition.face_encodings(known_image)[0])
    except IndexError:
        print("I wasn't able to locate any faces in at least one of the images. Check the image files. Aborting...")
        


    face_distances = face_recognition.face_distance(known_images_encoded, image_to_test_encoding)


    lowest_distance = face_distances[0]
    highest_distance = face_distances[0]
    lowest_match_index = 0
    highest_match_index = 0

    for i, face_distance in enumerate(face_distances):


        if face_distance > highest_distance:
            highest_distance = face_distance
            highest_match_index = i

        if face_distance < lowest_distance:
            lowest_distance = face_distance
            lowest_match_index = i


        #print("The test image has a distance of {:.2} from known image #{}".format(face_distance, i))
        #print("- With a normal cutoff of 0.6, would the test image match the known image? {}".format(face_distance < 0.6))
        #print("- With a very strict cutoff of 0.5, would the test image match the known image? {}".format(face_distance < 0.5))
        #print()

    #print('highest_distance:')
    #print(highest_distance)
    #print(filenames[lowest_match_index])

    #print('lowest_distance:')
    #print(lowest_distance)
    #print(filenames[highest_match_index])
    return filenames[lowest_match_index],filenames[highest_match_index]
    # Load a test image and get encondings for it

    # See how far apart the test image is from the known faces
def morph_image(image, output_directory='./'):
    lowest_distance_image, highest_distance_image = get_face_distances(image)
    #morph_images(lowest_distance_image, image, output_directory='./out/lowest/')
    return morph_images(highest_distance_image, image, output_directory=output_directory)

def morph_images(image1, image2, output_directory='./', mode=5):
    print('start morphing:')

    imgpaths= [
        image1,
        image2
    ]
    print(imgpaths)
    out_filename = output_directory+str(time.time())+'.png'
    if(mode == 1):
        img = facemorpher.averager(imgpaths, out_filename=out_filename,dest_filename=image2)
    if(mode == 2):
        img = facemorpher.averager(imgpaths, out_filename=out_filename,dest_filename=image2,blur_edges=True)
    if(mode == 3):
        img = facemorpher.averager(imgpaths, out_filename=out_filename,dest_filename=image2,alpha=True)
    if(mode == 4):
        img = facemorpher.averager(imgpaths, out_filename=out_filename,dest_filename=image2,alpha=True,blur_edges=True)
    if(mode == 5):
        print('run python ./FaceSwap/main.py --src '+image1+' --dst '+image2+'  --out '+out_filename+' --correct_color')
        #yeah, i know...
        os.system('cd FaceSwap && python ./main.py --src ../'+image1+' --dst ../'+image2+'  --out ../'+out_filename+' --no_debug_window --correct_color')

    if(mode == 6):
        print('run python ./FaceSwap/main.py --src '+image1+' --dst '+image2+'  --out '+out_filename+'')
        #yeah, i know...
        os.system('cd FaceSwap && python ./main.py --src ../'+image1+' --dst ../'+image2+'  --out ../'+out_filename+' --no_debug_window')

    return out_filename





# usage server.py (-t) in.jpg/or dir out.jpg or directory
# -t starts test

print('Number of arguments:', len(sys.argv), 'arguments.')
print('Argument List:', str(sys.argv))
if '-t' in sys.argv :
    print("starting test")
    print(sys.argv[2])
    print(sys.argv[3])
    run_tests(sys.argv[2],sys.argv[3])

    