#Facemorph API

##Install

Run:
1. `./install-virtual-env.sh`
2. `./create-virtual-env.sh`
3. `source facemorph-env/bin/activate`
4. `pip install -r requirements.txt`

##Start Server

`source facemorph-env/bin/activate`
`cd flask`
`export FLASK_APP=server.py`
`flask run`


## Description

The Script is following these steps:

1. Start a server

2. Wait for uploads

3. If image is uploaded
 3.1 Check if image contains face (yes proceed with step 4 else exit with error)

4. Check for facematches

5. Morph the picture with the best matching picture
    Using one of those techniques:
    https://github.com/wuhuikai/FaceSwap
    https://github.com/matthewearl/faceswap
    http://matthewearl.github.io/2015/07/28/switching-eds-with-python/

6. Add orginal image to the image directory



## Keep in mind

Possible TLS connection (nginx reverse proxy)

To reduce max image size:
convert -define jpeg:extent=900kb *.jpg
